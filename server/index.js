import express from "express";
import cors from "cors";

const port = 3000;
const app = express();
app.use(express.json());
app.use(cors());

const daten = [
    { id: "aaa", name: "Anna", phone: "+49891235543", gender: "w" },
    { id: "bbb", name: "Berta", phone: "0894444", gender: "d" },
    { id: "ccc", name: "Carla", phone: "3333333", gender: "w" },
    { id: "ddd", name: "Dieter", phone: "4444", gender: "m" }
]

// Server starten
app.listen(port, () => {
    console.log(`Webserver läuft auf Port ${port}.`);
});

// Endpoint "/"
app.get('/', (request, response) => {
    // Antwort schicken
    response.send('<h1>Servus!</h1><p>Hallo</p>');
});

//======================
// C R E A T E
//======================
// Neuen Kontakt erstellen
app.post('/contacts/', (request, response) => {
    const newContact = request.body;
    // Hier müsste noch überprüft werden,
    // ob "name" und "phone" angegeben ist
    newContact.id = crypto.randomUUID();
    daten.push(newContact);
    response.send("Kontakt wurde erstellt.");
});

//======================
// R E A D
//======================
// Alle Kontakte lesen
app.get('/contacts/', (request, response) => {
    // Antwort schicken
    response.json(daten);
});

// Einen speziellen Kontakt lesen
app.get('/contacts/:id', (request, response) => {
    // Request auswerten
    const id = request.params.id;

    const contact = daten.find(person => person.id === id);

    // Falls kein passender contact vorhanden,
    // hat contact den Wert 'undefinded'    
    if (contact) {
        // contact schicken
        response.json(contact);
    } else {
        // contact undefined
        // => 404 zurückschicken
        response
            .status(404)
            .send(`Tut mir leid. Ich konnte den Kontakt mir der id ${id} nicht finden.`);
    }
});

//======================
// U P D A T E 
//======================
// Einen vorhandenen Kontakt ändern
app.put('/contacts/:id', (request, response) => {
    // Request auswerten
    const id = request.params.id;

    const index = daten.findIndex(person => person.id === id);

    if (index >= 0) {
        // Kontakt gefunden
        const updatedContact = request.body;

        // Daten des Kontakts aktualisieren
        daten[index].name = updatedContact.name;
        daten[index].phone = updatedContact.phone;
        daten[index].gender = updatedContact.gender;

        // OK zurückschicken
        response.send('Kontakt erfolgreich bearbeitet');
    } else {
        // keinen passenden Kontakt gefunden
        // => 404 zurückschicken
        response
            .status(404)
            .send(`Tut mir leid. Ich konnte den Kontakt mir der id ${id} nicht finden.`);
    }
});

//======================
// D E L E T E
//======================
// Einen vorhandenen Kontakt löschen
app.delete('/contacts/:id', (request, response) => {
    // Request auswerten
    const id = request.params.id;

    // index ermitteln
    const index = daten.findIndex(contact => contact.id === id);

    if (index >= 0) {
        // passenden Kontakt gefunden

        // Element mit index aus Array 'daten' löschen
        daten.splice(index, 1);

        // Antwort zurückschicken
        response.send("Kontakt gelöscht");
    } else {
        // keinen passenden Kontakt gefunden
        // Antwort zurückschicken
        response
            .status(404)
            .send(`Tut mir leid. Ich konnte den Kontakt mir der id ${id} nicht finden.`);
    }
});
