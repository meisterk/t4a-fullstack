import { RestClient } from "./restclient";

// =====================
// D A T A + Read
// =====================
export let contactsInTable = $state([]);

// =====================
// GUI-Variables
// =====================
export let formVisible = $state({ state: "main" });
/* 
        'main': table "Kontakte"
        'delete': form "Kontakt löschen"
        'edit': form "Kontakt bearbeiten"
*/
export let contactToEdit = $state({});
export let contactToDelete = $state({
    id: "",
    name: ""
});

export const refreshTable = async () => {
    // Alle Daten vom Server holen
    const restClient = new RestClient();
    const allContacts = await restClient.getAllContacts();

    // Tabelle aktualisieren (contacts)
    // alten Inhalt löschen
    contactsInTable.length = 0;

    // allContacts -> contacts
    allContacts.forEach((contact) => {
        contactsInTable.push(contact);
    });
};
