export class RestClient {
    constructor() {
        this.url = 'http://localhost:3000/contacts/'
    }

    // C R E A T E
    async createContact(contact) {
        await fetch(this.url, {
            method: 'POST',
            headers: {
                //'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(contact)
        });
    }

    // R E A D
    async getAllContacts() {
        const response = await fetch(this.url);
        const allContacts = await response.json();
        return allContacts;
    }

    getContact(id) {
        // TODO
        return {};
    }

    // U P D A T E
    async updateContact(contact) {
        await fetch(`${this.url}${contact.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(contact)
        });
    }

    // D E L E T E
    async deleteContact(id) {
        await fetch(`${this.url}${id}`, {
            method: 'DELETE'
        });
    }
}